# !/usr/bin/env python
# -*- coding: utf-8 -*-
# writer:龚子谦
import random
# 抢普通红包案例
# 某群有多个成员，群主给成员发普通红包。发红包的规则是：
# 1、群主负责发红包。红包金额从群主余额中扣除，按成员人数平均分成n等份，以备领取。
# 2、成员负责抢红包。抢到的红包金额存到自己余额中。
# 3、抢完红包后需要进行报数，打印格式“我是XX，现在有 XX 块钱。”。
# 请根据描述信息，完成案例中所有类的定义，类之间的继承关系，以及发红包、抢红包的操作。
class Hongbao():
    # money=''#余额
    # hbmoney=''#红包金额
    # hbnumber=''#红包个数
    # name=''#姓名
    def __init__(self,name,money,hbmoney,hbnumber):
        self.money=money
        self.hbmoney=hbmoney
        self.hbnumber=hbnumber
        self.name=name

    def fahongbao(self):#发红包
        if self.hbmoney>0 and self.money>=self.hbmoney:
            print(f'我是群主{self.name}，发了{self.hbnumber}个红包共{self.hbmoney}元，剩余{self.money-self.hbmoney}元')
            self.money-=self.hbmoney#余额变化

        else:
            print('发红包失败')
            self.hbmoney=0#红包金额置零

    def grab_hongbao(self):#抢红包
        if self.hbmoney==0:
            print('NO hbmoney')
        elif self.hbnumber==1:
            grab_money=self.hbmoney
            print(f'我是{self.name},抢了红包{grab_money}元，现在有{self.money + grab_money}元')

        elif self.hbnumber>1:
            grab_money=random.randint(1,self.hbmoney)#随机整数红包
            print(f'我是{self.name},抢了红包{grab_money}元，现在有{self.money+grab_money}元')
            self.money+=grab_money#余额
            self.hbmoney-=grab_money#剩余红包金额
            self.hbnumber-=1#红包个数减一


if __name__ == '__main__':

    qunzhu=Hongbao('小白',10000,int(input('发红包金额为：')),3)#群主小白余额为10000元，发了3个红包共''元
    qunzhu.fahongbao()



    qunyuan1=Hongbao('jack',5000,qunzhu.hbmoney,qunzhu.hbnumber)#第一个抢
    qunyuan1.grab_hongbao()

    qunyuan2=Hongbao('tom',500000,qunyuan1.hbmoney,qunyuan1.hbnumber)#第二个抢
    qunyuan2.grab_hongbao()

    qunyuan3=Hongbao('sary',15000,qunyuan2.hbmoney,qunyuan2.hbnumber)#第三个抢
    qunyuan3.grab_hongbao()














