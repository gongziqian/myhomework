# !/usr/bin/env python
# -*- coding: utf-8 -*-
# writer:龚子谦
#写一个面向对象的例子：
# 比如创建一个类（Animal）【动物类】，类里有属性（名称，颜色，年龄，性别），类方法（会叫，会跑）
# 创建子类【猫】，继承【动物类】
# 重写父类的__init__方法，继承父类的属性
# 添加一个新的属性，毛发=短毛
# 添加一个新的方法， 会捉老鼠，
# 重写父类的【会叫】的方法，改成【喵喵叫】
# 创建子类【狗】，继承【动物类】
# 复写父类的__init__方法，继承父类的属性
# 添加一个新的属性，毛发=长毛
# 添加一个新的方法， 会看家
# 复写父类的【会叫】的方法，改成【汪汪叫】
# 在入口函数中创建类的实例
# 创建一个猫猫实例
# 调用捉老鼠的方法
# 打印【猫猫的姓名，颜色，年龄，性别，毛发，捉到了老鼠】
# 创建一个狗狗实例
# 调用【会看家】的方法
# 打印【狗狗的姓名，颜色，年龄，性别，毛发】
class Animal():
    def __init__(self,name,color,age,gender):
        self.name=name
        self.color=color
        self.age=age
        self.gender=gender
    def speak(self):
        print(f'{self.name}会叫')
    def run(self):
        print(f'{self.name}在跑')

class Cat(Animal):
    hair = '短毛'
    def __init__(self,name,color,age,gender):
        super().__init__(name,color,age,gender)

    def catchmice(self):
        print(f'{self.name}会抓老鼠')
    def speak(self):
        print(f'{self.name}喵喵叫')

class Dog(Animal):
    hair='长毛'
    def __init__(self, name, color, age, gender):
        super().__init__(name, color, age, gender)
    def watchhome(self):
        print(f'{self.name}会看家')
    def speak(self):
        print(f'{self.name}汪汪叫')


if __name__ == '__main__':
    cat1=Cat('小毛','黄色','17','公')
    cat1.catchmice()
    print(f'{cat1.name},颜色{cat1.color},年龄是{cat1.age},性别是{cat1.gender},毛发是{cat1.hair},捉到了老鼠')
    dog1=Dog('jack','red','18','female')
    dog1.watchhome()
    print(f'{dog1.name},{dog1.color},{dog1.age},{dog1.gender},{dog1.hair},会看家')






